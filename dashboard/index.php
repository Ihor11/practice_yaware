<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>overflow</title>
    <?php
        require 'Unirest.php';
    ?>
    <script type="text/javascript" src="fusioncharts/js/fusioncharts.js"></script>
    <script type="text/javascript" src="fusioncharts/js/themes/fusioncharts.theme.fint.js"></script>


</head>
<body>
<?php


function arithmeticAverage($response){
    $sum = getProductivityTimeSum($response);
    $emp_count = count($response->body);
    $avg = $sum/$emp_count;
    return $avg;
}

function getProductivityTimeSum($response){
    $sum_time = 0;
    foreach($response->body as $emp_time){
        $sum_time += $emp_time->seconds;
    }
    return $sum_time;
}

function secondsToTime($ss) {
    $s = $ss%60;
    $m = floor(($ss%3600)/60);
    $h = floor(($ss%86400)/3600);
    $d = floor(($ss%2592000)/86400);
    $M = floor($ss/2592000);

    return "$M months, $d days, $h hours, $m minutes, $s seconds";
}

$access_key = "943d5027a30a2800f53e3b62bcbf67e2-1293709475";


//Employees count

$response = Unirest\Request::post("https://api4yaware-yaware-data.p.mashape.com/account/json/v2/getEmployees?access_key=$access_key",
    array(
        "X-Mashape-Key" => "6slTeBpMJ3msh3TmSbByo67GFQXrp11U0y4jsnebsQw1yIVXes",
        "Content-Type" => "application/x-www-form-urlencoded",
        "Accept" => "application/json"
    )
);
$emp_count = count($response->body);


//Uncategorized time summary

$response = Unirest\Request::post("https://api4yaware-yaware-data.p.mashape.com/account/json/v2/getEmployeesProductivityTime?access_key=$access_key&dateFrom=2015-01-15&dateTo=2020-01-20&productivity[]=uncategorized",
    array(
        "X-Mashape-Key" => "6slTeBpMJ3msh3TmSbByo67GFQXrp11U0y4jsnebsQw1yIVXes",
        "Content-Type" => "application/x-www-form-urlencoded",
        "Accept" => "application/json"
    )
);

$uncategorized_time_sum = getProductivityTimeSum($response);


//Distracted time summary

$response = Unirest\Request::post("https://api4yaware-yaware-data.p.mashape.com/account/json/v2/getEmployeesProductivityTime?access_key=$access_key&dateFrom=2015-01-15&dateTo=2020-01-20&productivity[]=distracting",
    array(
        "X-Mashape-Key" => "6slTeBpMJ3msh3TmSbByo67GFQXrp11U0y4jsnebsQw1yIVXes",
        "Content-Type" => "application/x-www-form-urlencoded",
        "Accept" => "application/json"
    )
);

$distracted_time_sum = getProductivityTimeSum($response);


//Total non-productive time summary

$non_productive_time_sum = $uncategorized_time_sum + $distracted_time_sum;


//Productive time summary

$response = Unirest\Request::post("https://api4yaware-yaware-data.p.mashape.com/account/json/v2/getEmployeesProductivityTime?access_key=$access_key&dateFrom=2015-01-15&dateTo=2020-01-20&productivity[]=productive",
    array(
        "X-Mashape-Key" => "6slTeBpMJ3msh3TmSbByo67GFQXrp11U0y4jsnebsQw1yIVXes",
        "Content-Type" => "application/x-www-form-urlencoded",
        "Accept" => "application/json"
    )
);

$productive_time_sum = getProductivityTimeSum($response);


// Average work time

$average_work_time = $productive_time_sum / $emp_count;

?>
<div id="table">
    <table>
        <tr>
            <th>Employees:</th><td><?php echo $emp_count; ?></td>
        </tr>
        <tr>
            <th>Non-productive time sum:</th><td><?php echo secondsToTime($non_productive_time_sum); ?></td>
        </tr>
        <tr>
            <th>Time worked:</th><td><?php echo secondsToTime($productive_time_sum); ?></td>
        </tr>
        <tr>
            <th>Average :</th><td><?php echo secondsToTime($average_work_time); ?></td>
        </tr>
    </table>
</div>


<div id="chart-container">FusionCharts will render here</div>

<script>
    FusionCharts.ready(function () {
        var revenueChart = new FusionCharts({
            type: 'column3d',
            renderAt: 'chart-container',
            width: '500',
            height: '300',
            dataFormat: 'json',
            dataSource: {
                "chart": {
                    "caption": "Revenue for 2015-01-15 - 2020-01-20",
                    "subCaption": "Average working time: <?php echo secondsToTime($average_work_time); ?>",
                    "xAxisName": "Time",
                    "yAxisName": "Days",
                    "paletteColors": "#0075c2",
                    "valueFontColor": "#000000",
                    "baseFont": "Helvetica Neue,Arial",
                    "captionFontSize": "14",
                    "subcaptionFontSize": "14",
                    "subcaptionFontBold": "0",
                    "placeValuesInside": "1",
                    "rotateValues": "1",
                    "showShadow": "0",
                    "divlineColor": "#999999",
                    "divLineIsDashed": "1",
                    "divlineThickness": "1",
                    "divLineDashLen": "1",
                    "divLineGapLen": "1",
                    "canvasBgColor": "#ffffff"
                },

                "data": [
                    {
                        "label": "Productive time",
                        "value": "<?php echo $productive_time_sum/3600/24; ?>"
                    },
                    {
                        "label": "Uncategorized time",
                        "value": "<?php echo $uncategorized_time_sum/3600/24; ?>"
                    },
                    {
                        "label": "Distracted time",
                        "value": "<?php echo $distracted_time_sum/3600/24; ?>"
                    }
                ]
            }
        });
        revenueChart.render();
    });
</script>

</body>
</html>